package myjavalab;

/**
 * Created with IntelliJ IDEA.
 * User: Michael
 * Date: 18.01.14
 * Time: 15:31
 * To change this template use File | Settings | File Templates.
 */

/**Class Edge
 *
 */
public class Edge {

    private int x, y;

    /**
     * Public constructor
     *
     * @param x - value X
     * @param y - value Y
     */
    public Edge(int x, int y) {

        this.x = x;
        this.y = y;

    }

    /**Getter  x
     * @return x
     */
    public int getX() {
        return x;
    }

    /**Setter x
     * @param x
     */
    public void setX(int x) {
        this.x = x;
    }

    /**Getter y
     * @return y
     */
    public int getY() {
        return y;
    }

    /**Setter y
     * @param y
     */
    public void setY(int y) {
        this.y = y;
    }

    /**Override method equals for class Edge
     * @return false, if
     * @return true
     */
    @Override
    public boolean equals(Object edge){

        if(edge.getClass() != this.getClass()) return false;

        Edge e = (Edge) edge;

        if(e.getX() == this.getX() && e.getY() == this.getY())
            return true;

        else
            return false;
    }
}
