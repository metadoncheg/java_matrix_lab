package myjavalab; /**
 * Created with IntelliJ IDEA.
 * User: Michael
 * Date: 14.01.14
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */

public class Example {

    public Example() {}

    public static void main(String[] args) {

       if(args.length == 0 || args.length > 2){

           System.out.println("Invalid number of arguments!");
           return;
       }

       Matrix matrix = new Matrix(args[0]);

       if(matrix.isValid() != Matrix.Message.Accepted){

           System.out.println("No valid matrix!");
           return;
       }
       /// to do


       if(args.length == 1){

           System.out.print(matrix);
           return;
       }

       if(args.length == 2){

           matrix.writeInFile(args[1]);
           return;
       }


    }
}
