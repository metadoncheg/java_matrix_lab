package myjavalab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import static myjavalab.Matrix.Message.*;

/**
 * Created with IntelliJ IDEA.
 * User: Michael
 * Date: 15.01.14
 * Time: 17:59
 * To change this template use File | Settings | File Templates.
 */
public class Matrix {

    static enum Message {WrongFormat, WrongType, Accepted, WrongSize, Empty};

    private int[][] data;

    private int counter;

    public Matrix(String namefile){

        Scanner reader;
        String str;

        try {

            File infile = new File(namefile);

            reader = new Scanner(infile);

            str = reader.nextLine();

            if(str == null) {

                System.out.println("File is empty");

            }
            else {

                System.out.println("M size = " + Integer.parseInt(str)) ;

                data = new int[Integer.parseInt(str)][];

                counter = 0;

                while (reader.hasNext()){

                    this.populate(parseRow((reader.nextLine())));
                }

                //System.out.print(matrix);
               //System.out.println("Validating : " + matrix.isValid());
            }
        }
        catch (FileNotFoundException e) {

            e.printStackTrace();
        }

        catch (IOException e) {

        }

        catch (NumberFormatException e){

            System.out.println("Invalid input symbols");
        }

    }

    public void writeInFile(String filename){

        Matrix matrix = new Matrix(data);

        FileWriter fileWriter = null;

        try {

            fileWriter = new FileWriter("C:/output.txt");

            fileWriter.write(matrix.toString());

            fileWriter.close();
        }
        catch (IOException e) {

            e.printStackTrace();
        }

    }

    public Matrix(int[][] data) {

        this.data = new int[data.length][];

        for(int i = 0; i < data.length; i ++) {

            this.data[i] = new int[data[i].length];

            for(int j = 0; j < data[i].length; j ++) {

                this.data[i][j] = data[i][j];
            }
        }
    }

    public void populate(int[] row) {

        data[counter] = new int[row.length];

        for(int i = 0; i < row.length; i ++) data[counter][i] = row[i];

        counter ++;
    }

    public Message isValid() {

        if(!this.checkConsistence()) return WrongSize;

        if(this.isNull()) return Empty;

        if(!this.checkCorrectness()) return WrongFormat;

        if(this.checkLessThanZero()) return WrongType;

        return Accepted;
    }

    public boolean checkLessThanZero() {

        for(int i=0; i < data.length; i++){
            for(int j=0; j < data[i].length; j++){

                if(data[i][j] < 0) return true;
            }
        }

        return false;
    }

    public boolean checkCorrectness() {

        for(int i=0; i < data.length; i++){
            for(int j=0; j < data[i].length; j++){

                if(i == j) continue;

                if(data[i][j] != data[j][i]) return false;
            }
        }

        return true;
    }

    public boolean isNull() {

        for(int i = 0; i < data.length; i ++) {
            for(int j = 0; j < data[i].length; j ++) {

                if(data[i][j] != 0) return false;
            }
        }

        return true;
    }

    public boolean checkConsistence() {

        for(int i=0; i < data.length; i++){

            if(data[i].length != data.length) return false;
        }

        return true;
    }

    public List<Edge> getMaxElements(){

        if(this.isValid() != Accepted) System.out.println("Not valid");

        int etalon = 0; //counter = 0;

        for(int i=0; i < data.length; i++){
            for(int j=0; j < data[i].length; j++){

                //if(i == j) continue;

                if(data[i][j] == 0) continue;

                //if(data[i][j] == etalon) counter ++;

                if(data[i][j] > etalon){

                    etalon = data[i][j];
                    //counter = 1;
                }
            }
        }

        List<Edge> values = new ArrayList<Edge>();

        //counter = 0;

        for(int i=0; i < data.length; i++){
            for(int j=0; j < data[i].length; j++){

                //if(i == j) continue;

                if(data[i][j] == 0) continue;

                if(data[i][j] == etalon) {

                    values.add(new Edge(i, j));
                    //values[counter][1] = j;

                    //counter ++;
                }
            }
        }

        return values;
    }

    public List<Edge> getMinElements(){

        //if(this.isValid() != Message.Accepted) System.out.println("Not valid");

        int etalon = 0;

        for(int i=0; i < data.length; i++){
            for(int j=0; j < data[i].length; j++){

                if(data[i][j] != 0){

                    etalon = data[i][j];
                    break;
                }
            }
        }

        for(int i=0; i < data.length; i++){
            for(int j=0; j < data[i].length; j++){

                if(data[i][j] == 0) continue;

                if(data[i][j] < etalon){

                    etalon = data[i][j];
                }
            }
        }

        List<Edge> values = new ArrayList<Edge>();

        for(int i=0; i < data.length; i++){
            for(int j=0; j < data[i].length; j++){

                if(data[i][j] == 0) continue;

                if(data[i][j] == etalon) {

                    values.add(new Edge(i, j));
                }
            }
        }

        return values;
    }

    /**Method parseRow
     * Converts a string into an int[] with help StringTokenizer
     * Convert token == "-" => token == 0
     * With help parseInt we get element int[i]
     * @param str
     * @return int[]
     * @throws NumberFormatException
     */
    private int[] parseRow(String str) throws NumberFormatException{

        StringTokenizer tokenizer = new StringTokenizer(str, " ", false);

        String token;

        int[]  integerArray = new int[tokenizer.countTokens()];

        for(int i =0; i < integerArray.length; i++) {

            token = tokenizer.nextToken();

            if(token.equalsIgnoreCase("-")) {

                //System.out.println(token);

                integerArray[i] = 0;
            }

            else {

                integerArray[i] = Integer.parseInt(token);
                //integerList.add(Integer.parseInt(token));
            }

            //System.out.println(token);
        }

        return integerArray;
    }

    @Override
    public String toString() {

        String string = new String();

        for(int i=0; i < data.length; i++){
            for(int j=0; j < data[i].length; j++){

                string += "[" + data[i][j] + "]";

                if(j != data[i].length - 1) string += " ";

                else string += "\n";
            }
        }

        //string += "Validating : " + this.isValid() + "\n";

        Matrix.Message msg = this.isValid();

        switch(msg) {

            case WrongType:

                string += "Some values less than zero are found!";

                return string;

            case WrongFormat:

                string+= "Non symmetric matrix!";

                return string;

            case WrongSize:

                string+= "Not consistence matrix!";

                return string;

            case Empty:

                string+= "Matrix is empty!";

                return string;

        }

        List<Edge> v = this.getMaxElements();

        List<Edge> m = this.getMinElements();

        string += "[Max : " + data[v.get(0).getX()][v.get(0).getY()] + "]\n";

        string += "Edge : ";

        for(int i = 0; i < v.size(); i ++) {

            string += "(" + v.get(i).getX() + ", " + v.get(i).getY() + ")";

            if(i != v.size() - 1) string += ", ";

            else string += "\n";

        }

        string += "[Min : " + data[m.get(0).getX()][m.get(0).getY()] + "]\n";

        string+= "Edge: ";

        for(int i = 0; i < m.size(); i ++) {

            string += "(" + m.get(i).getX() + ", " + m.get(i).getY() + ")";

            if(i != m.size() - 1) string += ", ";

            else string += "\n";
        }

        return string;
    }
}
