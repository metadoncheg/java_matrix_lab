package myjavalab;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Michael
 * Date: 17.01.14
 * Time: 15:54
 * To change this template use File | Settings | File Templates.
 */
public class MatrixTest {

    @Test
    public void testIsValid() throws Exception {

        int[][] data = {
                {0, 1, 2},
                {1, 0, 4},
                {2, 4, 0}
        };

        int[][] data_declined = {
                {0, 1, 4},
                {1, 0, 1},
                {2, 4, 1}
        };

        Matrix m = new Matrix(data);
        Matrix m_declined = new Matrix(data_declined);

        assertEquals(m.isValid(), Matrix.Message.Accepted);
        assertEquals(m_declined.isValid(), Matrix.Message.WrongFormat);
    }

    @Test
    public void testGetMaxElements() throws Exception {

        int[][] data = {
                {0, 1, 2},
                {1, 0, 4},
                {2, 4, 0}
        };

        List<Edge> testList = new ArrayList<Edge>();
        Matrix m = new Matrix(data);

        testList.add(new Edge(1, 2));
        testList.add(new Edge(2, 1));

        assertEquals(m.getMaxElements(), testList);
    }

    @Test
    public void testGetMinElements() throws Exception {

        int[][] data = {
                {0, 1, 2},
                {1, 0, 4},
                {2, 4, 0}
        };

        List<Edge> testList = new ArrayList<Edge>();
        Matrix m = new Matrix(data);

        testList.add(new Edge(0, 1));
        testList.add(new Edge(1, 0));

        assertEquals(m.getMinElements(), testList);
    }

    @Test
    public void testCheckConsistence() throws Exception{

        int[][] data = {
                {0, 1, 2},
                {1, 0, 4},
                {2, 4, 0, 5}
        };

        Matrix m = new Matrix(data);

        assertEquals(m.checkConsistence(),false);

    }

    @Test
    public void testCheckCorrectness(){

        int[][] data = {
                {0, 1, 3},
                {2, 0, 4},
                {3, 4, 0}
        };

        Matrix m = new Matrix(data);

        assertEquals(m.checkCorrectness(), false);
    }

    @Test
    public void testIsNull(){

        int[][] data = {
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0}
        };

        Matrix m = new Matrix(data);

        assertEquals(m.isNull(), true);
    }

    @Test
    public void testCheckLessThanZero(){

        int[][] data = {
                {0, 1, -3},
                {2, 0, 4},
                {3, 4, 0}
        };

        Matrix m = new Matrix(data);

        assertEquals(m.checkLessThanZero(), true);

    }
}
